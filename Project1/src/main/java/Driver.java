import com.Service.ServiceHandler;

import io.javalin.Javalin;

public class Driver {

	public static void main(String[] args) {
		Javalin app =Javalin.create(config ->{
			config.addStaticFiles("/Frontend");
		});
		ServiceHandler sh=new ServiceHandler();
		app.start(9010);
		app.post("/login",sh.validateLogin);
		app.get("/employee", sh.getEmployeesReimb);
		app.post("/employee", sh.createReimb);
		app.delete("/employee", sh.logOut);
		app.get("/manager", sh.getManagerReimbs);
		app.put("/manager", sh.updateReimb);
		app.delete("/manager", sh.logOut);
	}
}