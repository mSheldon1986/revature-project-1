package com.model;

public class Role {
	private int roleId;
	private String role;
	
	public Role() {
	}
	
	
	
	public Role(int roleId) throws Exception {
		super();
		this.roleId = roleId;
		switch(roleId) {
		case 1:
			this.role="EMPLOYEE";
			break;
		case 2: 
			this.role="MANAGER";
			break;
		default:
			throw new Exception();
		}
	}

	

	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", role=" + role + "]";
	}


	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
