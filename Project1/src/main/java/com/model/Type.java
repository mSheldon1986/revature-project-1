package com.model;

public class Type {
	private int id;
	private String type;
	
	public Type() {
		// TODO Auto-generated constructor stub
	}

	public Type(int id) {
		super();
		this.id = id;
		switch(id) {
		case 1:
			type="LODGING";
			break;
		case 2:
			type="TRAVEL";
			break;
		case 3:
			type="FOOD";
			break;
		case 4:
			type="OTHER";
			break;
		default:
				
		}
	}

	@Override
	public String toString() {
		return "Type [id=" + id + ", type=" + type + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
