package com.model;

public class Status {
	private int statusId;
	private String status;
	
public Status() {
	
}

public Status(int statusId) {
	super();
	this.statusId = statusId;
	switch(statusId) {
	case -1:
		status="REJECTED";
		break;
	case 0:
		status="PENDING";
		break;
	case 1:
		status="ACCEPTED";
		break;
	default:
		status="";
	}
}

@Override
public String toString() {
	return "Status [statusId=" + statusId + ", status=" + status + "]";
}

public int getStatusId() {
	return statusId;
}

public void setStatusId(int statusId) {
	this.statusId = statusId;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

}
