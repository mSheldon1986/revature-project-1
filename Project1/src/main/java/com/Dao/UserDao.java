package com.Dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.*;

public class UserDao{
	private DBConnection connection;
	
	public UserDao() {
	}
	
	
	
	
	public UserDao(DBConnection connection) {
		super();
		this.connection = connection;
	}

	public boolean validatePassword(User user) {
		User check=findByName(user.getUsername());
		if(check==null)
			return false;
		if(check.compareTo(user)==0)
			return true;
		return false;
	}

	
	public User findByName(String name) {
		User toRet=null;
		Connection c=connection.getConnection();
		try {
			CallableStatement theCall=c.prepareCall("{ call get_user_by_name(?) }");
			theCall.setString(1, name);
			ResultSet res=theCall.executeQuery();
			if(res.next()) {
				toRet=new User(res.getInt("user_id"),res.getString("user_username"),res.getString("user_password"),res.getString("user_firstname"),res.getString("user_lastname"),res.getString("user_email"),new Role(res.getInt("user_roleid")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return toRet;
	}
	
	public User findByID(int id) {
		User toRet=null;
		Connection c=connection.getConnection();
		try {
			CallableStatement theCall=c.prepareCall("{ call get_user_by_id(?) }");
			theCall.setInt(1, id);
			ResultSet res=theCall.executeQuery();
			if(res.next()) {
				toRet=new User(res.getInt("user_id"),res.getString("user_username"),res.getString("user_password"),res.getString("user_firstname"),res.getString("user_lastname"),res.getString("user_email"),new Role(res.getInt("user_roleid")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return toRet;
	}
	
}
