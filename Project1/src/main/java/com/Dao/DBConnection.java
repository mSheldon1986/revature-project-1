package com.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private String URL= "jdbc:mariadb://database-1.ci41ldtkkpez.us-east-2.rds.amazonaws.com:3306/project1db";
	private String name ="oneuser";
	private String password="password";
	
	public Connection getConnection() {
		Connection c=null;
		try {
			c=DriverManager.getConnection(URL,name,password);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	
}
