package com.Dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.model.*;

public class ReimbursementDao  {
	private DBConnection connection;
	public ReimbursementDao() {
		
	}
	
	
	public ReimbursementDao(DBConnection connection) {
		super();
		this.connection = connection;
	}


	public List<Reimbursement> findByName(User us) {
		List<Reimbursement> toRet=new ArrayList<Reimbursement>();
		Connection c= connection.getConnection();
		UserDao ud=new UserDao(new DBConnection());
		try {
			CallableStatement cs=c.prepareCall("{ call get_reimb_by_author(?) }");
			cs.setString(1, us.getUsername());
			ResultSet rs=cs.executeQuery();
			while(rs.next()) {
				toRet.add(new Reimbursement(
						rs.getInt("reimb_id"),
						rs.getInt("reimb_amount"),
						rs.getDate("reimb_submitted"),
						rs.getDate("reimb_resolved"),
						rs.getString("reimb_description"),
						ud.findByID(rs.getInt("reimb_author")),
						ud.findByID(rs.getInt("reimb_resolver")),
						new Status(rs.getInt("reimb_status_id")),
						new Type(rs.getInt("reimb_type_id"))
						));
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		return toRet;
		
	}
	
	public void createReimbursement(Reimbursement entry) {
		Connection c= connection.getConnection();
		try {
			CallableStatement cs=c.prepareCall("{ call create_reimbursement(?,?,?,?,?) }");
			cs.setInt(1, entry.getAmount());
			cs.setDate(2, entry.getSubmitted());
			cs.setString(3, entry.getDescription());
			cs.setInt(4, entry.getAuthor().getId());
			cs.setInt(5, entry.getType().getId());
			cs.execute();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void upDateReimbursement(User resolver,int id, int status) {
		Calendar cal=Calendar.getInstance();
		Connection c= connection.getConnection();
		try {
			CallableStatement cs=c.prepareCall("{ call update_reimb(?,?,?,?) }");
			cs.setInt(1, resolver.getId());
			cs.setInt(2, id);
			cs.setInt(3, status);
			cs.setDate(4, new Date( cal.getTimeInMillis()));
			cs.execute();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
	}
	public List<Reimbursement> getAllReimbursements(){
		ArrayList<Reimbursement> toRet=new ArrayList<Reimbursement>();
		Connection c= connection.getConnection();
		UserDao ud=new UserDao(new DBConnection());
		try {
			CallableStatement cs=c.prepareCall("{ call get_all_reimbursements() }");
			ResultSet rs=cs.executeQuery();
			while(rs.next()) {
				toRet.add(new Reimbursement(rs.getInt("reimb_id"),
						rs.getInt("reimb_amount"),
						rs.getDate("reimb_submitted"),
						rs.getDate("reimb_resolved"),
						rs.getString("reimb_description"),
						ud.findByID(rs.getInt("reimb_author")),
						ud.findByID(rs.getInt("reimb_resolver")),
						new Status(rs.getInt("reimb_status_id")),
						new Type(rs.getInt("reimb_type_id"))
						));
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		
		return toRet;
	}


}
