package com.Dao;

import java.sql.Date;
import java.util.Calendar;

import com.model.Reimbursement;
import com.model.Status;
import com.model.Type;
import com.model.User;

public class DaoTesting {

	public static void main(String[] args) {
		Calendar cal=Calendar.getInstance();
		UserDao ud=new UserDao(new DBConnection());
		ReimbursementDao rd =new ReimbursementDao(new DBConnection());
		User u=ud.findByName("Employee1");
		User m=ud.findByName("ihatelongnames");
		System.out.println(u);
		System.out.println(ud.validatePassword(u));
		u.setPassword("b2");
		System.out.println(ud.validatePassword(u));
		Reimbursement rbus=new Reimbursement(
				20, 
				500, 
				new Date(cal.getTimeInMillis()),
				null, 
				"I did More Stuff", 
				u,
				null, 
				new Status(0), 
				new Type(3)
				);
		rd.createReimbursement(rbus);
		System.out.println(rd.getAllReimbursements());
		rd.upDateReimbursement(m, 1001, -1);
		System.out.println(rd.getAllReimbursements());
	}

}
