package com.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.Dao.DBConnection;
import com.Dao.ReimbursementDao;
import com.Dao.UserDao;
import com.model.Reimbursement;
import com.model.Status;
import com.model.Type;
import com.model.User;

import io.javalin.http.Handler;

public class ServiceHandler {
	ReimbursementDao rd;
	UserDao ud;
	private static User currUser;
	public final static Logger log=Logger.getLogger(ServiceHandler.class);
	
	public ServiceHandler() {
		rd=new ReimbursementDao(new DBConnection());
		ud=new UserDao(new DBConnection());
		currUser=null;
	}
	

	public Handler validateLogin=(ctx)-> {
		User u=new User();
		u.setUsername(ctx.formParam("username"));
		u.setPassword(ctx.formParam("password"));
		boolean verify=ud.validatePassword(u);
		log.info("Login Attempt by:"+u.getUsername());
		if(verify) {
			ctx.status(200);
			ServiceHandler.currUser=ud.findByName(u.getUsername());
			switch(currUser.getRole().getRoleId()) {
			case 1:
				ctx.redirect("http://localhost:9010/html/employee.html");
				break;
			case 2:
				ctx.redirect("http://localhost:9010/html/manager.html");
			}
		}
		else {
			ctx.status(300);
			ctx.redirect("http://localhost:9010/html/login.html");
		}
	};
	public Handler getEmployeesReimb=(ctx)->{
		if(ServiceHandler.currUser==null || ServiceHandler.currUser.getRole().getRoleId()!=1) {
			return;
		}
		List<Reimbursement> theList=rd.findByName(currUser);
		ctx.json(theList);
		ctx.status(200);
	};
	public Handler getManagerReimbs=(ctx)->{
		if(ServiceHandler.currUser==null || ServiceHandler.currUser.getRole().getRoleId()!=2) {
			return;
		}
		List<Reimbursement> theList=rd.getAllReimbursements();
		ctx.json(theList);
		ctx.status(200);
	};
	public Handler createReimb=(ctx)->{
		Calendar cal=Calendar.getInstance();
		Reimbursement r=new Reimbursement(
				0, 
				Integer.parseInt(ctx.formParam("amount")), 
				new Date(cal.getTimeInMillis()), 
				null, 
				ctx.formParam("description"), 
				ServiceHandler.currUser,
				null, 
				new Status(0), 
				new Type(Integer.parseInt(ctx.formParam("type")))
				);
		rd.createReimbursement(r);
		ctx.status(200);
		ctx.redirect("http://localhost:9010/html/employee.html");
	};
	public Handler updateReimb=(ctx)->{
		rd.upDateReimbursement(currUser, Integer.parseInt(ctx.formParam("id")), Integer.parseInt(ctx.formParam("status")));
	};
	public Handler logOut=(ctx)->{
		ServiceHandler.currUser=null;
		ctx.status(200);
	};
}
