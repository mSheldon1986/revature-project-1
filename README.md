# Revature Project 1

## Description
The Expense Reimbursement System manages the process of reimbursing employees for expenses incurred during company business. All employees can log on and request reimbursement for expenses incurred. These expensesare then reviewed by a manager and approved or denied according to company policy. Employees can see all of their past reimbursement requests Managers can see all employees past and current reimbursement requests.

## Technologies Used
- Java
- HTML
- Javascript
- CSS
- Bootstrap
- Javalin
- Maven
- SQL
- RDS

## Features
- Secure login that prevents people that aren't logged in from accessing pages.
- Employees can login, logout, submit reimbursements requests, and view past reimbursement requests.
- Managers can login, logout, approve reimbursement requests, and deny reimbursement requests.

## Todo
- Implement unit tests for front and back ends.
- Hash Passwords for additional security.
- Implement sorting of reimbursements 
- Implement view by category
